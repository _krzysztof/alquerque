**Alquerque** is a simple game simmilar to checkers, written in c++ as
console aplication for the University purpouses in 2016. It's also my first big project.
The application allows you to play in a two-person and one-person mode 
(with a computer).
To make a move, the current player must type the coordinates of the pawn he wants to move and the place he wants to move it.
The program also have basic functions to write an actual state of the game to file and read from file.