﻿

#include<iostream>
#include<string>
#include<windows.h>
#include<fstream>
#include "funkcje.h"
#include "Gracz.h"

using namespace std;


class Game
{
protected:
	int gracz;		//ktory 0 to O, gracz 1 to X
	int tura;		//ktora tura+ sprawdzenie ktory gracz
	int gra;
	int wybor;
	int flaga, pom1, pom2;		//do ustalania, czy zachodzi bicie wielokrotne
	char m;
public:


	Game() : gracz(0), tura(0), gra(1), flaga(0)
	{
		Gracz *wsk;
		Czlowiek p1;
		Komputer k1;
		wsk = &k1;		//bez znaczenia ktory
	////////////////////////
		while (1)
		{
			m = menu();
			if (m == '1')		//komputer
			{
				wsk = &k1;
				wsk->set_tryb(2);
				break;
			}
			else
				if (m == '2')		//czlowiek
				{
					wsk = &p1;
					wsk->set_tryb(1);
					break;
				}
				else
					if (m == '3')		//odczyt
					{
						if (wsk->odczyt_trybu() == 1)
						{
							wsk = &p1;
						}
						else if (wsk->odczyt_trybu() == 2)
						{
							wsk = &k1;
						}
						else
						{
							cout << "Plik nie istnieje" << endl;
							cout << "nacisnij enter...";
							while (getchar() != '\n')
								;
							continue;
						}
						gracz = wsk->odczyt_gry();
						if (gracz == 5)
						{
							gracz = 0;
							continue;
						}
						if (gracz == 1) tura = 1;	//by gracz odpowiednio sie zmienial
						break;
					}
					else
						if (m == '4')				//zasady
						{
							zasady();
							cout << endl << "nacisnij enter...";
							while (getchar() != '\n')
								;
						}
						else
							if (m == '5')			//pomoc
							{
								pomoc();
								cout << endl << "nacisnij enter...";
								while (getchar() != '\n')
									;
							}
							else
								if (m == '6')		//wyniki
								{
									if (wsk->odczyt_wyniku() == 5) continue;
									cout << endl << "nacisnij enter...";
									while (getchar() != '\n')
										;
								}
		}

		while (gra != 0)							//glowna petla gry
		{
			wsk->wypisz();
			//
			cout << "Gracz " << gracz + 1;
			if (gracz == 0) cout << " : O" << endl;
			else if (gracz == 1) cout << " : X" << endl;
			//
			//////////////////     MIEJSCE NA KOMPUTER     //////////////////////////////////////////
			if (wsk->get_tryb() == 2 && gracz == 1)
			{
				if (k1.ruch_losowy(gracz) == 1)
				{
					wsk->pionkiO--;
					while (k1.podwojne_bicie_komputer(gracz) == 1)
					{
						wsk->pionkiO--;
					}
				}
				tura++;
				gracz = tura % 2;
				if (wsk->pionkiO == 0) gra = 0;
				continue;
			}
			//////////////////////////////\//////////////////////////////////////////

			wsk->kontrola_poprawnosci_danych(gracz);		//wprowadzenie danych
			//
			if (flaga == 1)	//jesli jest podwojne bicie
			{
				if (wsk->get_z1() != pom1 && wsk->get_z2() != pom2)
				{
					cout << "Masz drugie bicie, mozesz sie ruszyc tylko tym samym pionkiem" << endl;
					cout << "nacisnij enter...";
					while (getchar() != '\n')
						;
					continue;
				}
			}
			flaga = 0;
			//
			if (wsk->zgodnosc_bicia(wsk->get_z1(), wsk->get_z2(), wsk->get_n1(), wsk->get_n2()) == 1)		// bicie
			{
				if (wsk->wprowadz_bicie(gracz) != 1)
				{
					cout << "Bicie nieprawidlowe" << endl;
					cout << "nacisnij enter...";
					while (getchar() != '\n')
						;
					continue;
				}
				if (gracz == 0)wsk->pionkiX--;
				if (gracz == 1)wsk->pionkiO--;
				//jesli jest podwojne bicie
				if (wsk->podwojne_bicie(gracz) == 1)
				{
					pom1 = wsk->get_z1();		//do flagi
					pom2 = wsk->get_z2();		//do flagi
					flaga = 1;					//jesli flaga jest wlaczona, bicie moze nastapic tylko z tego samego miejsca
					continue;
				}
				//		
			}
			else
				if (wsk->zgodnosc_ruchu() == 1)		//ruch
				{
					if (wsk->sprawdz_czy_bicie(gracz) == 1)
					{
						cout << "Musisz bic!" << endl;
						cout << "nacisnij enter...";
						while (getchar() != '\n')
							;
						continue;
					}
					else
					{
						if (wsk->wprowadz_ruch(gracz) != 1)
						{
							cout << "Ruch nieprawidlowy" << endl;
							cout << "nacisnij enter...";
							while (getchar() != '\n')
								;
							continue;
						}
					}
				}
				else
				{
					cout << "BLAD, JESZCZE RAZ" << endl;
					cout << "nacisnij enter...";
					while (getchar() != '\n')
						;
					continue;
				}
			////
			tura++;
			gracz = tura % 2;
			if (wsk->pionkiO == 0 || wsk->pionkiX == 0) gra = 0;
		}
		//////////////////////////////////////////////

		cout << endl << "KONIEC GRY" << endl;
		if (wsk->pionkiO == 0)
		{
			cout << "Wagral Gracz nr 2!" << endl;
			wsk->zapis_wyniku(2);
		}
		if (wsk->pionkiX == 0)
		{
			cout << "Wagral Gracz nr 1!" << endl;
			wsk->zapis_wyniku(1);
		}

		wsk->odczyt_wyniku();
	}

};



int main()
{
	Game();

	return 0;
}


/////////////////////////////////////////////////////////////////////////////
