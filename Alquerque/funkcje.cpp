#include<iostream>
#include<string>
#include<windows.h>
#include"funkcje.h"

using namespace std;


int menu()
{
	system("cls");
	char wybor;
	int ok = 0;
	cout << endl << "MENU: " << endl << endl;
	cout << "Gra jednoosobowa : 1" << endl;
	cout << "Gra dwuosobowa : 2" << endl;
	cout << "Wznow gre : 3" << endl;
	cout << "Zasady gry : 4" << endl;
	cout << "Pomoc : 5" << endl;
	cout << "Wyniki : 6" << endl;
	cout << "Wyjscie (w dowolnym momencie gry) : 0" << endl;

	while (ok != 1)
	{
		wybor = getchar();
		if (wybor == '0')
		{
			exit(0);
		}

		if (!isdigit(wybor))
		{
			cout << "Blad, musisz podac cyfre" << endl;
			ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}

		if (wybor > '0' && wybor <= '6')
		{
			ok = 1;
			while (getchar() != '\n')
				;
		}
		else
		{
			cout << "Blad, jeszcze raz" << endl;
			ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
	}
	return wybor;
}

void zasady()
{
	cout << endl << "Celem gry jest zbicie jak najwiekszej liczby pionkow przeciwnika." << endl;
	cout << "W grze bierze udzial dwoch graczy. Kazdy z nich ma zestaw 12 pionkow." << endl;
	cout << "Pionki poruszaja sie przez przechodzenie wzdluz linii na najblizsze" << endl << "dowolne wolne pole." << endl;
	cout << "Bicie zachodzi przez przeskoczenie po linii na wolne pole za sasiednim" << endl << "pionkiem przeciwnika." << endl;
	cout << "Mozliwe jest bicie wielokrotne dokonywane w dowolnym kierunku." << endl << "Bicie jest obowiazkowe." << endl;
}

void pomoc()
{
	cout << endl << "Wspolrzedne nalezy wpisywac w nastepujacy sposob :" << endl;
	cout << "Najpierw litera i cyfra z miejsca, z ktorego chcemy sie ruszyc," << endl;
	cout << "nastepnie : spacja, slowo \"na\" spacja," << endl;
	cout << "i litera i cyfra miejsca, na ktore chcemy sie ruszyc." << endl;
	cout << "Powinno to wygladac w nastepujacy sposob :" << endl << "(litera 1)(cyfra 1) na (litera 2)(cyfra 2) " << endl;

}