///////////////////////////////////////////

#include<iostream>
#include<string>
#include<windows.h>
#include<fstream>
#include"Gracz.h"


char Gracz::get_z1() { return z1; }
char Gracz::get_z2() { return z2; }
char Gracz::get_n1() { return n1; }
char Gracz::get_n2() { return n2; }
int Gracz::get_tryb() { return tryb; }
void Gracz::set_tryb(int pom) { tryb = pom; }



Gracz::Gracz() : pionkiX(12), pionkiO(12)
{
	//uzupelnianie pola
	for (i = 0; i <= 4; i++)
	{
		for (j = 0; j <= 4; j++)
		{
			if (i <= 2)
			{
				if (i == 2 && j > 2)
				{
					x[i][j] = 'O';
					continue;
				}
				x[i][j] = 'X';
			}
			else if (i >= 3)
			{
				x[i][j] = 'O';
			}
		}
	}
	x[2][2] = '.';
}

void Gracz::wypisz()
{
	system("cls");
	printf("\n");
	printf("    A	    B	    C	    D	    E\n\n");
	printf(" 1  %c_______%c_______%c_______%c_______%c\n", x[0][0], x[0][1], x[0][2], x[0][3], x[0][4]);
	printf("    | .     |     . | .     |     . |\n");
	printf("    |   .   |   .   |   .   |   .   |\n");
	printf("    |     . | .     |     . | .     |\n");
	printf(" 2  %c_______%c_______%c_______%c_______%c\n", x[1][0], x[1][1], x[1][2], x[1][3], x[1][4]);
	printf("    |     . | .     |     . | .     |\n");
	printf("    |   .   |   .   |   .   |   .   |\n");
	printf("    | .     |     . | .     |     . |\n");
	printf(" 3  %c_______%c_______%c_______%c_______%c\n", x[2][0], x[2][1], x[2][2], x[2][3], x[2][4]);
	printf("    | .     |     . | .     |     . |\n");
	printf("    |   .   |   .   |   .   |   .   |\n");
	printf("    |     . | .     |     . | .     |\n");
	printf(" 4  %c_______%c_______%c_______%c_______%c \n", x[3][0], x[3][1], x[3][2], x[3][3], x[3][4]);
	printf("    |     . | .     |     . | .     |\n");
	printf("    |   .   |   .   |   .   |   .   |\n");
	printf("    | .     |     . | .     |     . |\n");
	printf(" 5  %c_______%c_______%c_______%c_______%c \n\n", x[4][0], x[4][1], x[4][2], x[4][3], x[4][4]);
}

void Gracz::kontrola_poprawnosci_danych(int gracz)
{
	jest_ok = 0;
	//kontrola poprawnosci danych
	while (jest_ok != 1)
	{
		cout << "podaj wspolrzedne w sposob : a1 na b2" << endl;
		//1 znak litera
		znak = getchar();
		//jesli gracz chce przerwac gre
		if (znak == '0')
		{
			cout << "Jesli na pewno chcesz wyjsc, nacisnij 1" << endl;
			cout << "Jesli nie chcesz wyjsc, wybierz 0" << endl;
			cin >> wybor;
			if (wybor == 1)
			{
				zapis_trybu();		//zeby mozna bylo ustawic wskaznik p.
				zapis_gry(gracz, pionkiO, pionkiX);
				exit(0);
			}
			else
				if (wybor == 0)
				{
					jest_ok = 0;
					while (getchar() != '\n')
						;
					continue;
				}
				else
				{
					cout << "Blad" << endl;
					jest_ok = 0;
					while (getchar() != '\n')
						;
					continue;
				}
		}
		if (znak == '\n')			//jesli uzytkownik nacisnie enter zbyt wczesnie
		{
			cout << "Za malo danych!" << endl;
			continue;
		}
		if (!isalpha(znak))
		{
			cout << "Drugi znak musi byc litera!" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
		if (isupper(znak))
		{
			if (znak < 'A' || znak > 'E')	//sprawdzenie czy miescie sie w przedziale
			{
				cout << "Drugi znak musi byc litera od A do E!" << endl;
				jest_ok = 0;
				while (getchar() != '\n')
					;
				continue;
			}
			z2 = znak - 'A';			//zamiana na cyfre
		}
		else
		{
			if (znak < 'a' || znak > 'e')	//sprawdzenie czy miescie sie w przedziale
			{
				cout << "Drugi znak musi byc litera od A do E!" << endl;
				jest_ok = 0;
				while (getchar() != '\n')
					;
				continue;
			}
			z2 = znak - 'a';			//zamiana na cyfre
		}

		//2 znak cyfra
		znak = getchar();
		if (!isdigit(znak))
		{
			if (znak == '\n')
			{
				cout << "Nic nie podales!" << endl;
				continue;
			}
			cout << "Pierwszy znak musi byc cyfra" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
		if (znak < '1' || znak>'5')
		{
			cout << "Pierwszy znak musi byc cyfra OD 1 DO 5" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}

		z1 = znak - '0' - 1;		//+1, bo tablica jest od 0 do 4, tu nastepuje zamiana

		//
		//3 znak = spacja

		znak = getchar();
		if (znak == '\n')
		{
			cout << "Za malo danych!" << endl;
			continue;
		}

		if (znak != ' ')
		{
			cout << "Trzeci znak musi byc spacja" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
		//
		//4 znak = "n"
		znak = getchar();
		if (znak == '\n')
		{
			cout << "Za malo danych!" << endl;
			continue;
		}

		if (znak != 'n')
		{
			cout << "Czwarty znak musi byc \"n\"!" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
		//5 znak = "a"
		znak = getchar();

		if (znak == '\n')
		{
			cout << "Za malo danych!" << endl;
			continue;
		}

		if (znak != 'a')
		{
			cout << "Piaty znak musi byc \"a\"!" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
		//
		//6 znak = spacja
		znak = getchar();
		if (znak == '\n')
		{
			cout << "Za malo danych!" << endl;
			continue;
		}
		if (znak != ' ')
		{
			cout << "Szosty znak musi byc spacja" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
		//7 znak
		znak = getchar();
		if (znak == '\n')
		{
			cout << "Za malo danych!" << endl;
			continue;
		}
		if (!isalpha(znak))
		{

			cout << "Siodmy znak musi byc litera od A do E!" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
		if (isupper(znak))
		{
			if (znak < 'A' || znak > 'E')	//sprawdzenie czy miescie sie w przedziale
			{
				cout << "siodmy znak musi byc litera od A do E!" << endl;
				jest_ok = 0;
				while (getchar() != '\n')
					;
				continue;
			}
			n2 = znak - 'A';			//zamiana na cyfre
		}
		else
		{
			if (znak < 'a' || znak > 'e')	//sprawdzenie czy miescie sie w przedziale
			{
				cout << "siodmy znak musi byc litera od A do E!" << endl;
				jest_ok = 0;
				while (getchar() != '\n')
					;
				continue;
			}
			n2 = znak - 'a';			//zamiana na cyfre
		}

		//8 znak
		znak = getchar();

		if (znak == '\n')
		{
			cout << "Za malo danych!" << endl;
			continue;
		}

		if (!isdigit(znak))
		{
			if (znak == '\n')
			{
				cout << "Nic nie podales!" << endl;

				continue;
			}
			cout << "Osmy znak musi byc cyfra" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}
		if (znak < '1' || znak>'5')
		{
			cout << "Osmy znak musi byc cyfra OD 1 DO 5" << endl;
			jest_ok = 0;
			while (getchar() != '\n')
				;
			continue;
		}

		n1 = znak - '0' - 1;		//+1, bo tablica jest od 0 do 4, tu nastepuje zamiana


									//
		jest_ok = 1;
		while (getchar() != '\n')
			;
		continue;
	}	//koniec while (jest_ok==1)
}

int Gracz::zgodnosc_ruchu()
{
	zgodnoscR = 0;
	//// WLASCIWE PORUSZANIE SIE NA PLANSZY /////

	if (z2 == n2)						//ruch pionowy
	{
		if (z1 - n1 == 1 || z1 - n1 == -1)
		{
			zgodnoscR = 1;
		}
	}
	else
		if (z1 == n1)				//ruch poziomy
		{
			if (z2 - n2 == 1 || z2 - n2 == -1)
			{
				zgodnoscR = 1;
			}
		}
		else
			if (z1 == z2 && n1 == n2)			//przekatna glowna w dol 
			{
				if (z1 - n1 == 1 || z1 - n1 == -1)
				{
					zgodnoscR = 1;
				}
			}
			else
				if (z1 + z2 == 4 && n1 + n2 == 4)			//przekatna glowna w gore
				{
					if (z1 - n1 == 1 || z1 - n1 == -1)
					{
						zgodnoscR = 1;
					}
				}
				else
					if ((z1 == 0 && z2 == 2 || z1 == 1 && z2 == 3 || z1 == 2 && z2 == 4) &&
						(n1 == 0 && n2 == 2 || n1 == 1 && n2 == 3 || n1 == 2 && n2 == 4)) //przekatna gorna w dol
					{
						if (z1 - n1 == 1 || z1 - n1 == -1)
						{
							zgodnoscR = 1;
						}
					}
					else
						if ((z1 == 2 && z2 == 0 || z1 == 3 && z2 == 1 || z1 == 4 && z2 == 2) &&
							(n1 == 2 && n2 == 0 || n1 == 3 && n2 == 1 || n1 == 4 && n2 == 2)) //przekatna dolna w dol
						{
							if (z1 - n1 == 1 || z1 - n1 == -1)
							{
								zgodnoscR = 1;
							}
						}
						else
							if ((z1 == 2 && z2 == 0 || z1 == 1 && z2 == 1 || z1 == 0 && z2 == 2) &&
								(n1 == 2 && n2 == 0 || n1 == 1 && n2 == 1 || n1 == 0 && n2 == 2)) //przekatna gorna w gore
							{
								if (z1 - n1 == 1 || z1 - n1 == -1)
								{
									zgodnoscR = 1;
								}
							}
							else
								if ((z1 == 4 && z2 == 2 || z1 == 3 && z2 == 3 || z1 == 2 && z2 == 4) &&
									(n1 == 4 && n2 == 2 || n1 == 3 && n2 == 3 || n1 == 2 && n2 == 4)) //przekatna dolna w gore
								{
									if (z1 - n1 == 1 || z1 - n1 == -1)
									{
										zgodnoscR = 1;
									}
								}
	return zgodnoscR;
}

int Gracz::zgodnosc_bicia(char z1, char z2, char n1, char n2)
{
	zgodnoscB = 0;
	//// WLASCIWE PORUSZANIE SIE NA PLANSZY /////

	if (z2 == n2)						//ruch pionowy
	{
		if (z1 - n1 == 2 || z1 - n1 == -2)
		{
			zgodnoscB = 1;
		}
	}
	else
		if (z1 == n1)				//ruch poziomy
		{
			if (z2 - n2 == 2 || z2 - n2 == -2)
			{
				zgodnoscB = 1;
			}
		}
		else
			if (z1 == z2 && n1 == n2)			//przekatna glowna w dol 
			{
				if (z1 - n1 == 2 || z1 - n1 == -2)
				{
					zgodnoscB = 1;
				}
			}
			else
				if (z1 + z2 == 4 && n1 + n2 == 4)			//przekatna glowna w gore
				{
					if (z1 - n1 == 2 || z1 - n1 == -2)
					{
						zgodnoscB = 1;
					}
				}
				else
					if ((z1 == 0 && z2 == 2 || z1 == 1 && z2 == 3 || z1 == 2 && z2 == 4) &&
						(n1 == 0 && n2 == 2 || n1 == 1 && n2 == 3 || n1 == 2 && n2 == 4)) //przekatna gorna w dol
					{
						if (z1 - n1 == 2 || z1 - n1 == -2)
						{
							zgodnoscB = 1;
						}
					}
					else
						if ((z1 == 2 && z2 == 0 || z1 == 3 && z2 == 1 || z1 == 4 && z2 == 2) &&
							(n1 == 2 && n2 == 0 || n1 == 3 && n2 == 1 || n1 == 4 && n2 == 2)) //przekatna dolna w dol
						{
							if (z1 - n1 == 2 || z1 - n1 == -2)
							{
								zgodnoscB = 1;
							}
						}
						else
							if ((z1 == 2 && z2 == 0 || z1 == 1 && z2 == 1 || z1 == 0 && z2 == 2) &&
								(n1 == 2 && n2 == 0 || n1 == 1 && n2 == 1 || n1 == 0 && n2 == 2)) //przekatna gorna w gore
							{
								if (z1 - n1 == 2 || z1 - n1 == -2)
								{
									zgodnoscB = 1;
								}
							}
							else
								if ((z1 == 4 && z2 == 2 || z1 == 3 && z2 == 3 || z1 == 2 && z2 == 4) &&
									(n1 == 4 && n2 == 2 || n1 == 3 && n2 == 3 || n1 == 2 && n2 == 4)) //przekatna dolna w gore
								{
									if (z1 - n1 == 2 || z1 - n1 == -2)
									{
										zgodnoscB = 1;
									}
								}
	return zgodnoscB;
}

int Gracz::sprawdz_czy_bicie(int gracz)
{
	bicie = 0;
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 5; j++)
		{
			if (gracz == 0)
			{
				if (x[i][j] == 'O')
				{
					if (x[i - 1][j - 1] == 'X' && x[i - 2][j - 2] == '.' && !(i - 2 < 0 || j - 2 < 0))
					{
						if (zgodnosc_bicia(i, j, i - 2, j - 2) == 1)
						{
							bicie = 1;
						}
					}
					else
						if (x[i - 1][j - 0] == 'X' && x[i - 2][j - 0] == '.' && !(i - 2 < 0))
						{

							if (zgodnosc_bicia(i, j, i - 2, j) == 1)
							{
								bicie = 1;
							}
						}
						else
							if (x[i - 1][j + 1] == 'X' && x[i - 2][j + 2] == '.' && !(i - 2 < 0 || j + 2 > 4))
							{

								if (zgodnosc_bicia(i, j, i - 2, j + 2) == 1)
								{
									bicie = 1;
								}
							}
							else
								if (x[i - 0][j - 1] == 'X' && x[i - 0][j - 2] == '.' && !(j - 2 < 0))
								{

									if (zgodnosc_bicia(i, j, i, j - 2) == 1)
									{
										bicie = 1;
									}
								}
								else
									if (x[i - 0][j + 1] == 'X' && x[i - 0][j + 2] == '.' && !(j + 2 > 4))
									{

										if (zgodnosc_bicia(i, j, i, j + 2) == 1)
										{
											bicie = 1;
										}
									}
									else
										if (x[i + 1][j - 1] == 'X' && x[i + 2][j - 2] == '.' && !(i + 2 > 4 || j - 2 < 0))
										{

											if (zgodnosc_bicia(i, j, i + 2, j - 2) == 1)
											{
												bicie = 1;
											}
										}
										else
											if (x[i + 1][j - 0] == 'X' && x[i + 2][j - 0] == '.' && !(i + 2 > 4))
											{

												if (zgodnosc_bicia(i, j, i + 2, j) == 1)
												{
													bicie = 1;
												}
											}
											else
												if (x[i + 1][j + 1] == 'X' && x[i + 2][j + 2] == '.' && !(i + 2 > 4 || j + 2 > 4))
												{

													if (zgodnosc_bicia(i, j, i + 2, j + 2) == 1)
													{
														bicie = 1;
													}
												}
				}
			}
			if (gracz == 1)
			{
				if (x[i][j] == 'X')
				{

					if (x[i - 1][j - 1] == 'O' && x[i - 2][j - 2] == '.' && !(i - 2 < 0 || j - 2 < 0))
					{

						if (zgodnosc_bicia(i, j, i - 2, j - 2) == 1)
						{
							bicie = 1;
						}
					}
					else
						if (x[i - 1][j - 0] == 'O' && x[i - 2][j - 0] == '.' && !(i - 2 < 0))
						{

							if (zgodnosc_bicia(i, j, i - 2, j - 0) == 1)
							{
								bicie = 1;
							}
						}
						else
							if (x[i - 1][j + 1] == 'O' && x[i - 2][j + 2] == '.' && !(i - 2 < 0 || j + 2 > 4))
							{

								if (zgodnosc_bicia(i, j, i - 2, j + 2) == 1)
								{
									bicie = 1;
								}
							}
							else
								if (x[i - 0][j - 1] == 'O' && x[i - 0][j - 2] == '.' && !(j - 2 < 0))
								{

									if (zgodnosc_bicia(i, j, i, j - 2) == 1)
									{
										bicie = 1;
									}
								}
								else
									if (x[i - 0][j + 1] == 'O' && x[i - 0][j + 2] == '.' && !(j + 2 > 4))
									{

										if (zgodnosc_bicia(i, j, i, j + 2) == 1)
										{
											bicie = 1;
										}
									}
									else
										if (x[i + 1][j - 1] == 'O' && x[i + 2][j - 2] == '.' && !(i + 2 > 4 || j - 2 < 0))
										{

											if (zgodnosc_bicia(i, j, i + 2, j - 2) == 1)
											{
												bicie = 1;
											}
										}
										else
											if (x[i + 1][j - 0] == 'O' && x[i + 2][j - 0] == '.' && !(i + 2 > 4))
											{

												if (zgodnosc_bicia(i, j, i + 2, j - 0) == 1)
												{
													bicie = 1;
												}
											}
											else
												if (x[i + 1][j + 1] == 'O' && x[i + 2][j + 2] == '.' && !(i + 2 > 4 || j + 2 > 4))
												{

													if (zgodnosc_bicia(i, j, i + 2, j + 2) == 1)
													{
														bicie = 1;
													}

												}
				}
			}
		}
	}
	return bicie;
}

int Gracz::podwojne_bicie(int gracz)
{
	flaga = 0;
	bicie = 0;
	z1 = i = n1;
	z2 = j = n2;
	if (gracz == 0)
	{
		if (x[i][j] == 'O')
		{
			if (x[i - 1][j - 1] == 'X' && x[i - 2][j - 2] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i - 2, j - 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i - 2 < 0 || j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 1][j - 0] == 'X' && x[i - 2][j - 0] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j;
				if (zgodnosc_bicia(i, j, i - 2, j) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 1][j + 1] == 'X' && x[i - 2][j + 2] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i - 2, j + 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i - 1 < 0 || j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 0][j - 1] == 'X' && x[i - 0][j - 2] == '.' &&flaga != 1)
			{
				n1 = i;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i, j - 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 0][j + 1] == 'X' && x[i - 0][j + 2] == '.' && flaga != 1)
			{
				n1 = i;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i, j + 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j - 1] == 'X' && x[i + 2][j - 2] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i + 2, j - 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4 || j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j - 0] == 'X' && x[i + 2][j - 0] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j;
				if (zgodnosc_bicia(i, j, i + 2, j) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j + 1] == 'X' && x[i + 2][j + 2] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i + 2, j + 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4 || j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}
		}
	}
	if (gracz == 1)
	{
		if (x[i][j] == 'X')
		{

			if (x[i - 1][j - 1] == 'O' && x[i - 2][j - 2] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i - 2, j - 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i - 2 < 0 || j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 1][j - 0] == 'O' && x[i - 2][j - 0] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j;
				if (zgodnosc_bicia(i, j, i - 2, j - 0) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 1][j + 1] == 'O' && x[i - 2][j + 2] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i - 2, j + 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i - 2 < 0 || j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 0][j - 1] == 'O' && x[i - 0][j - 2] == '.' && flaga != 1)
			{
				n1 = i;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i, j - 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 0][j + 1] == 'O' && x[i - 0][j + 2] == '.' && flaga != 1)
			{
				n1 = i;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i, j + 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j - 1] == 'O' && x[i + 2][j - 2] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i + 2, j - 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4 || j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j - 0] == 'O' && x[i + 2][j - 0] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j;
				if (zgodnosc_bicia(i, j, i + 2, j - 0) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j + 1] == 'O' && x[i + 2][j + 2] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i + 2, j + 2) == 1)
				{
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4 || j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}
		}
	}
	return bicie;
}

int Gracz::wprowadz_ruch(int gracz)
{
	jest_ok = 0;
	if (gracz == 1)
	{
		if (x[z1][z2] == 'X' && x[n1][n2] == '.')
		{
			x[n1][n2] = 'X';
			x[z1][z2] = '.';
			jest_ok = 1;
			wypisz();
		}
	}
	else
		if (gracz == 0)
		{
			if (x[z1][z2] == 'O' && x[n1][n2] == '.')
			{
				x[n1][n2] = 'O';
				x[z1][z2] = '.';
				jest_ok = 1;
				wypisz();
			}
		}

	return jest_ok;
}

int Gracz::wprowadz_bicie(int gracz)
{
	jest_ok = 0;

	if (x[z1][z2] == 'X' && gracz == 1)
	{
		if (x[z1 - 1][z2 - 1] == 'O' && x[z1 - 2][z2 - 2] == '.' && n1 == z1 - 2 && n2 == z2 - 2)
		{
			x[z1 - 1][z2 - 1] = '.';
			x[z1 - 2][z2 - 2] = 'X';
			x[z1][z2] = '.';
			jest_ok = 1;
		}
		else
			if (x[z1 - 1][z2 - 0] == 'O' && x[z1 - 2][z2 - 0] == '.' && n1 == z1 - 2 && n2 == z2 - 0)
			{
				x[z1 - 1][z2 - 0] = '.';
				x[z1 - 2][z2 - 0] = 'X';
				x[z1][z2] = '.';
				jest_ok = 1;
			}
			else
				if (x[z1 - 1][z2 + 1] == 'O' && x[z1 - 2][z2 + 2] == '.' && n1 == z1 - 2 && n2 == z2 + 2)
				{
					x[z1 - 1][z2 + 1] = '.';
					x[z1 - 2][z2 + 2] = 'X';
					x[z1][z2] = '.';
					jest_ok = 1;
				}
				else
					if (x[z1 - 0][z2 - 1] == 'O' && x[z1 - 0][z2 - 2] == '.' && n1 == z1 - 0 && n2 == z2 - 2)
					{
						x[z1 - 0][z2 - 1] = '.';
						x[z1 - 0][z2 - 2] = 'X';
						x[z1][z2] = '.';
						jest_ok = 1;
					}
					else if (x[z1 - 0][z2 + 1] == 'O' && x[z1 - 0][z2 + 2] == '.' && n1 == z1 - 0 && n2 == z2 + 2)
					{
						x[z1 - 0][z2 + 1] = '.';
						x[z1 - 0][z2 + 2] = 'X';
						x[z1][z2] = '.';
						jest_ok = 1;
					}
					else
						if (x[z1 + 1][z2 - 1] == 'O' && x[z1 + 2][z2 - 2] == '.' && n1 == z1 + 2 && n2 == z2 - 2)
						{
							x[z1 + 1][z2 - 1] = '.';
							x[z1 + 2][z2 - 2] = 'X';
							x[z1][z2] = '.';
							jest_ok = 1;
						}
						else
							if (x[z1 + 1][z2 - 0] == 'O' && x[z1 + 2][z2 - 0] == '.' && n1 == z1 + 2 && n2 == z2 - 0)
							{
								x[z1 + 1][z2 - 0] = '.';
								x[z1 + 2][z2 - 0] = 'X';
								x[z1][z2] = '.';
								jest_ok = 1;
							}
							else
								if (x[z1 + 1][z2 + 1] == 'O' && x[z1 + 2][z2 + 2] == '.' && n1 == z1 + 2 && n2 == z2 + 2)
								{
									x[z1 + 1][z2 + 1] = '.';
									x[z1 + 2][z2 + 2] = 'X';
									x[z1][z2] = '.';
									jest_ok = 1;
								}

	}

	if (x[z1][z2] == 'O' && gracz == 0)
	{
		if (x[z1 - 1][z2 - 1] == 'X' && x[z1 - 2][z2 - 2] == '.' && n1 == z1 - 2 && n2 == z2 - 2)
		{
			x[z1 - 1][z2 - 1] = '.';
			x[z1 - 2][z2 - 2] = 'O';
			x[z1][z2] = '.';
			jest_ok = 1;
		}
		else
			if (x[z1 - 1][z2 - 0] == 'X' && x[z1 - 2][z2 - 0] == '.' && n1 == z1 - 2 && n2 == z2 - 0)
			{
				x[z1 - 1][z2 - 0] = '.';
				x[z1 - 2][z2 - 0] = 'O';
				x[z1][z2] = '.';
				jest_ok = 1;
			}
			else
				if (x[z1 - 1][z2 + 1] == 'X' && x[z1 - 2][z2 + 2] == '.' && n1 == z1 - 2 && n2 == z2 + 2)
				{
					x[z1 - 1][z2 + 1] = '.';
					x[z1 - 2][z2 + 2] = 'O';
					x[z1][z2] = '.';
					jest_ok = 1;
				}
				else
					if (x[z1 - 0][z2 - 1] == 'X' && x[z1 - 0][z2 - 2] == '.' && n1 == z1 - 0 && n2 == z2 - 2)
					{
						x[z1 - 0][z2 - 1] = '.';
						x[z1 - 0][z2 - 2] = 'O';
						x[z1][z2] = '.';
						jest_ok = 1;
					}
					else if (x[z1 - 0][z2 + 1] == 'X' && x[z1 - 0][z2 + 2] == '.' && n1 == z1 - 0 && n2 == z2 + 2)
					{
						x[z1 - 0][z2 + 1] = '.';
						x[z1 - 0][z2 + 2] = 'O';
						x[z1][z2] = '.';
						jest_ok = 1;
					}
					else
						if (x[z1 + 1][z2 - 1] == 'X' && x[z1 + 2][z2 - 2] == '.' && n1 == z1 + 2 && n2 == z2 - 2)
						{
							x[z1 + 1][z2 - 1] = '.';
							x[z1 + 2][z2 - 2] = 'O';
							x[z1][z2] = '.';
							jest_ok = 1;
						}
						else
							if (x[z1 + 1][z2 - 0] == 'X' && x[z1 + 2][z2 - 0] == '.' && n1 == z1 + 2 && n2 == z2 - 0)
							{
								x[z1 + 1][z2 - 0] = '.';
								x[z1 + 2][z2 - 0] = 'O';
								x[z1][z2] = '.';
								jest_ok = 1;
							}
							else
								if (x[z1 + 1][z2 + 1] == 'X' && x[z1 + 2][z2 + 2] == '.' && n1 == z1 + 2 && n2 == z2 + 2)
								{
									x[z1 + 1][z2 + 1] = '.';
									x[z1 + 2][z2 + 2] = 'O';
									x[z1][z2] = '.';
									jest_ok = 1;
								}
	}
	return jest_ok;
}


void Gracz::zapis_wyniku(int wygrany)
{
	fstream tablica_wynikow;
	tablica_wynikow.open("tablica wynikow.txt", ios::out | ios::app);
	tablica_wynikow << wygrany << endl;
	tablica_wynikow.close();
}

int Gracz::odczyt_wyniku()
{
	fstream tablica_wynikow;
	int gracz_x = 0, gracz_o = 0;
	tablica_wynikow.open("tablica wynikow.txt", ios::in);

	if (tablica_wynikow.good() == true)
	{
		string linia;

		cout << "-------------------" << endl;
		cout << "Gracz O  |  Gracz X" << endl;
		cout << "-------------------" << endl;
		while (getline(tablica_wynikow, linia))
		{
			if (atoi(linia.c_str()) == 1)
			{
				cout << "    1    |    0" << endl;
				gracz_o++;
			}

			if (atoi(linia.c_str()) == 2)
			{
				cout << "    0    |    1" << endl;
				gracz_x++;
			}
		}
		cout << "-------------------" << endl;
		cout << "Gracz nr 1 wygral " << gracz_o << " razy" << endl;
		cout << "Gracz nr 2 wygral " << gracz_x << " razy" << endl;
	}
	else
	{
		cout << "Plik nie istnieje" << endl;
		gracz_o = 5;		//specjalna wartosc
		cout << "Nacisnij enter...";
		while (getchar() != '\n');
		;
	}


	tablica_wynikow.close();

	return gracz_o;
}

void Gracz::zapis_gry(int gracz, int pionkiO, int pionkiX)
{
	fstream zapis;
	zapis.open("zapis gry.txt", ios::out);

	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 5; j++)
		{
			zapis << x[i][j] << " ";
		}
		zapis << endl;
	}
	zapis << gracz << endl;
	zapis << pionkiO << endl;	//stan pionkow
	zapis << pionkiX;			//
	zapis.close();
}

int Gracz::odczyt_gry()
{
	int ktory_gracz;
	fstream odczyt;
	odczyt.open("zapis gry.txt", ios::in);

	if (odczyt.good() == true)
	{
		for (i = 0; i < 5; i++)
		{
			for (j = 0; j < 5; j++)
			{
				odczyt >> x[i][j];
				if (x[i][j] != 'X' && x[i][j] != 'O' && x[i][j] != '.')	//jesli plik jest niezgodny z wymaganiami
				{
					cout << "Plik uszkodzony" << endl;
					cout << endl << "nacisnij enter...";
					while (getchar() != '\n')
						;
					return 5;
				}
			}
		}
		odczyt >> ktory_gracz;
		odczyt >> pionkiO;
		odczyt >> pionkiX;

		if (ktory_gracz != 1 && ktory_gracz != 0 || pionkiO > 12 || pionkiO < 0 || pionkiX > 12 || pionkiX < 0) //jesli plik jest niezgodny z wymaganiami
		{
			cout << "Plik uszkodzony" << endl;
			cout << endl << "nacisnij enter...";
			while (getchar() != '\n')
				;
			return 5;
		}
	}
	else
	{
		cout << "Plik nie istnieje" << endl;
		ktory_gracz = 5;		//specjalna wartosc
		cout << endl << "nacisnij enter...";
		while (getchar() != '\n')
			;
	}
	odczyt.close();
	return ktory_gracz;
}



void Gracz::zapis_trybu()
{
	fstream zapis;
	zapis.open("tryb.txt", ios::out);

	zapis << tryb;
	zapis.close();
}

int Gracz::odczyt_trybu()
{
	int flaga;
	fstream odczyt;
	odczyt.open("tryb.txt", ios::in);

	if (odczyt.good() == true)
	{
		odczyt >> tryb;
		flaga = tryb;
	}
	else
	{
		flaga = 5;		//specjalna wartosc
	}
	odczyt.close();
	return flaga;
}


////

Komputer::Komputer()
{
	tryb = 2;
}

int Komputer::ruch_losowy(int gracz)
{
	bicie = 0;
	//sprawdzanie czy jest bicie
	for (i = 0; i < 5; i++)
	{
		if (bicie == 1) break;
		for (j = 0; j < 5; j++)
		{
			if (gracz == 1 && x[i][j] == 'X')
			{
				if (x[i - 1][j - 1] == 'O' && x[i - 2][j - 2] == '.' && !(i - 2 < 0 || j - 2 < 0))
				{
					z1 = i;
					z2 = j;
					n1 = i - 2;
					n2 = j - 2;

					if (zgodnosc_bicia(z1, z2, n1, n2) == 1)
					{
						x[i - 1][j - 1] = '.';
						x[i - 2][j - 2] = 'X';
						x[i][j] = '.';
						bicie = 1;
						break;
					}
				}
				else
					if (x[i - 1][j - 0] == 'O' && x[i - 2][j - 0] == '.' && !(i - 2 < 0))
					{

						z1 = i;
						z2 = j;
						n1 = i - 2;
						n2 = j;

						if (zgodnosc_bicia(z1, z2, n1, n2) == 1)
						{
							x[i - 1][j - 0] = '.';
							x[i - 2][j - 0] = 'X';
							x[i][j] = '.';
							bicie = 1;
							break;
						}
					}
					else
						if (x[i - 1][j + 1] == 'O' && x[i - 2][j + 2] == '.' && !(i - 2 < 0 || j + 2 > 4))
						{
							z1 = i;
							z2 = j;
							n1 = i - 2;
							n2 = j + 2;

							if (zgodnosc_bicia(z1, z2, n1, n2) == 1)
							{
								x[i - 1][j + 1] = '.';
								x[i - 2][j + 2] = 'X';
								x[i][j] = '.';
								bicie = 1;
								break;
							}
						}
						else
							if (x[i - 0][j - 1] == 'O' && x[i - 0][j - 2] == '.' && !(j - 2 < 0))
							{
								z1 = i;
								z2 = j;
								n1 = i;
								n2 = j - 2;

								if (zgodnosc_bicia(z1, z2, n1, n2) == 1)
								{
									x[i - 0][j - 1] = '.';
									x[i - 0][j - 2] = 'X';
									x[i][j] = '.';
									bicie = 1;
									break;
								}
							}
							else
								if (x[i - 0][j + 1] == 'O' && x[i - 0][j + 2] == '.' && !(j + 2 > 4))
								{
									z1 = i;
									z2 = j;
									n1 = i;
									n2 = j + 2;

									if (zgodnosc_bicia(z1, z2, n1, n2) == 1)
									{
										x[i - 0][j + 1] = '.';
										x[i - 0][j + 2] = 'X';
										x[i][j] = '.';
										bicie = 1;
										break;
									}
								}
								else
									if (x[i + 1][j - 1] == 'O' && x[i + 2][j - 2] == '.' && !(i + 2 > 4 || j - 2 < 0))
									{
										z1 = i;
										z2 = j;
										n1 = i + 2;
										n2 = j - 2;

										if (zgodnosc_bicia(z1, z2, n1, n2) == 1)
										{
											x[i + 1][j - 1] = '.';
											x[i + 2][j - 2] = 'X';
											x[i][j] = '.';
											bicie = 1;
											break;
										}
									}
									else
										if (x[i + 1][j - 0] == 'O' && x[i + 2][j - 0] == '.' && !(i + 2 > 4))
										{
											z1 = i;
											z2 = j;
											n1 = i + 2;
											n2 = j;

											if (zgodnosc_bicia(z1, z2, n1, n2) == 1)
											{
												x[i + 1][j - 0] = '.';
												x[i + 2][j - 0] = 'X';
												x[i][j] = '.';
												bicie = 1;
												break;
											}
										}
										else
											if (x[i + 1][j + 1] == 'O' && x[i + 2][j + 2] == '.' && !(i + 2 > 4 || j + 2 > 4))
											{
												z1 = i;
												z2 = j;
												n1 = i + 2;
												n2 = j + 2;

												if (zgodnosc_bicia(z1, z2, n1, n2) == 1)
												{
													x[i + 1][j + 1] = '.';
													x[i + 2][j + 2] = 'X';
													x[i][j] = '.';
													bicie = 1;
													break;
												}
											}
			}
		}
	}
	//jesli nie ma bicia // mozna wykasowac elsy tak jak wyzej
	flaga = 0;
	if (bicie != 1)
	{
		for (i = 0; i < 5; i++)
		{
			if (flaga == 1) break;
			for (j = 0; j < 5; j++)
			{
				if (gracz == 1 && x[i][j] == 'X')
				{	////
					if (x[i - 1][j - 1] == '.')
					{
						z1 = i;
						z2 = j;
						n1 = i - 1;
						n2 = j - 1;
						if (i - 1 < 0 || j - 1 < 0) continue;
						if (zgodnosc_ruchu() == 1)
						{
							x[i - 1][j - 1] = 'X';
							x[i][j] = '.';
							flaga = 1;
							break;
						}
					}
					else
						if (x[i - 1][j - 0] == '.')
						{
							z1 = i;
							z2 = j;
							n1 = i - 1;
							n2 = j;
							if (i - 1 < 0) continue;
							if (zgodnosc_ruchu() == 1)
							{
								x[i - 1][j - 0] = 'X';

								x[i][j] = '.';
								flaga = 1;
								break;
							}
						}
						else
							if (x[i - 1][j + 1] == '.')
							{
								z1 = i;
								z2 = j;
								n1 = i - 1;
								n2 = j + 1;
								if (i - 2 < 0 || j + 2 > 4) continue;
								if (zgodnosc_ruchu() == 1)
								{
									x[i - 1][j + 1] = 'X';

									x[i][j] = '.';
									flaga = 1;
									break;
								}
							}
							else
								if (x[i - 0][j - 1] == '.')
								{
									z1 = i;
									z2 = j;
									n1 = i;
									n2 = j - 1;
									if (j - 1 < 0) continue;
									if (zgodnosc_ruchu() == 1)
									{
										x[i - 0][j - 1] = 'X';
										x[i][j] = '.';
										flaga = 1;
										break;
									}

								}
								else if (x[i - 0][j + 1] == '.')
								{
									z1 = i;
									z2 = j;
									n1 = i;
									n2 = j + 1;
									if (j + 1 > 4) continue;
									if (zgodnosc_ruchu() == 1)
									{
										x[i - 0][j + 1] = 'X';
										x[i][j] = '.';
										flaga = 1;
										break;
									}

								}
								else
									if (x[i + 1][j - 1] == '.')
									{
										z1 = i;
										z2 = j;
										n1 = i + 1;
										n2 = j - 1;
										if (i + 1 > 4 || j - 1 < 0) continue;
										if (zgodnosc_ruchu() == 1)
										{
											x[i + 1][j - 1] = 'X';
											x[i][j] = '.';
											flaga = 1;
											break;
										}

									}
									else
										if (x[i + 1][j - 0] == '.')
										{
											z1 = i;
											z2 = j;
											n1 = i + 1;
											n2 = j;
											if (i + 1 > 4) continue;
											if (zgodnosc_ruchu() == 1)
											{
												x[i + 1][j - 0] = 'X';
												x[i][j] = '.';
												flaga = 1;
												break;
											}

										}
										else
											if (x[i + 1][j + 1] == '.')
											{
												z1 = i;
												z2 = j;
												n1 = i + 1;
												n2 = j + 1;
												if (i + 1 > 4 || j + 1 > 4) continue;
												if (zgodnosc_ruchu() == 1)
												{
													x[i + 1][j + 1] = 'X';
													x[i][j] = '.';
													flaga = 1;
													break;
												}

											}
				}
			}
		}
	}
	return bicie;
}

int Komputer::podwojne_bicie_komputer(int gracz)
{
	flaga = 0;
	bicie = 0;
	z1 = i = n1;
	z2 = j = n2;
	if (gracz == 1)
	{
		if (x[i][j] == 'X')
		{

			if (x[i - 1][j - 1] == 'O' && x[i - 2][j - 2] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i - 2, j - 2) == 1 && !(i - 2 < 0 || j - 2 < 0))
				{
					x[i - 1][j - 1] = '.';
					x[i - 2][j - 2] = 'X';
					x[i][j] = '.';
					bicie = 1;
					flaga = 1;
				}
				if (i - 2 < 0 || j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 1][j - 0] == 'O' && x[i - 2][j - 0] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j;
				if (zgodnosc_bicia(i, j, i - 2, j - 0) == 1 && !(i - 2 < 0))
				{
					x[i - 1][j - 0] = '.';
					x[i - 2][j - 0] = 'X';
					x[i][j] = '.';
					bicie = 1;
					flaga = 1;
				}
				if (i - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 1][j + 1] == 'O' && x[i - 2][j + 2] == '.' && flaga != 1)
			{
				n1 = i - 2;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i - 2, j + 2) == 1 && !(i - 2 < 0 || j + 2 > 4))
				{
					x[i - 1][j + 1] = '.';
					x[i - 2][j + 2] = 'X';
					x[i][j] = '.';
					bicie = 1;
					flaga = 1;
				}
				if (i - 2 < 0 || j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 0][j - 1] == 'O' && x[i - 0][j - 2] == '.' && flaga != 1)
			{
				n1 = i;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i, j - 2) == 1 && !(j - 2 < 0))
				{
					x[i - 0][j - 1] = '.';
					x[i - 0][j - 2] = 'X';
					x[i][j] = '.';
					bicie = 1;
					flaga = 1;
				}
				if (j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i - 0][j + 1] == 'O' && x[i - 0][j + 2] == '.' && flaga != 1)
			{
				n1 = i;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i, j + 2) == 1 && !(j + 2 > 4))
				{
					x[i - 0][j + 1] = '.';
					x[i - 0][j + 2] = 'X';
					x[i][j] = '.';
					bicie = 1;
					flaga = 1;
				}
				if (j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j - 1] == 'O' && x[i + 2][j - 2] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j - 2;
				if (zgodnosc_bicia(i, j, i + 2, j - 2) == 1 && !(i + 2 > 4 || j - 2 < 0))
				{
					x[i + 1][j - 1] = '.';
					x[i + 2][j - 2] = 'X';
					x[i][j] = '.';
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4 || j - 2 < 0)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j - 0] == 'O' && x[i + 2][j - 0] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j;
				if (zgodnosc_bicia(i, j, i + 2, j - 0) == 1 && !(i + 2 > 4))
				{
					x[i + 1][j - 0] = '.';
					x[i + 2][j - 0] = 'X';
					x[i][j] = '.';
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}

			if (x[i + 1][j + 1] == 'O' && x[i + 2][j + 2] == '.' && flaga != 1)
			{
				n1 = i + 2;
				n2 = j + 2;
				if (zgodnosc_bicia(i, j, i + 2, j + 2) == 1 && !(i + 2 > 4 || j + 2 > 4))
				{
					x[i + 1][j + 1] = '.';
					x[i + 2][j + 2] = 'X';
					x[i][j] = '.';
					bicie = 1;
					flaga = 1;
				}
				if (i + 2 > 4 || j + 2 > 4)
				{
					bicie = 0;
					flaga = 0;
				}
			}
		}
	}
	return bicie;
}


////

Czlowiek::Czlowiek()
{
	tryb = 1;
}




