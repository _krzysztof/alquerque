#pragma once

#include<iostream>
#include<string>
#include<windows.h>
#include<fstream>


using namespace std;


class Gracz //bazowa
{
protected:
	int i, j;
	int x[5][5];		//do inicjalizowania planszy
	//
	char z1, z2, n1, n2, znak;
	int jest_ok;
	//
	int zgodnoscR;		// zgodnosc ruchu
	int zgodnoscB;		//zgodnosc bicia
	int bicie;
	int flaga;
	int tryb;			// tryb 1 - czlowiek, tryb 2 - komputer

public:

	int pionkiX, pionkiO;		//ilosc pionkow graczy na planszy
	int wybor;

	char get_z1();
	char get_z2();
	char get_n1();
	char get_n2();
	int get_tryb();
	void set_tryb(int pom);

	Gracz();

	void wypisz();											//wypisywanie planszy na ekran

	void kontrola_poprawnosci_danych(int gracz);			//wprowadzanie danych + sprawdzanie poprawnosci

	int zgodnosc_ruchu();									//sprawdzanie czy ruch jest zgodny z zasadami

	int zgodnosc_bicia(char z1, char z2, char n1, char n2);	//sprawdzanie, czy bicie moze zostac wykonane

	int sprawdz_czy_bicie(int gracz);						//sprawdzanie, czy gracz ma bicie

	int podwojne_bicie(int gracz);							//sprawdzanie, czy gracz ma podwojne bicie

	int wprowadz_ruch(int gracz);							// wprowadzenie ruchu

	int wprowadz_bicie(int gracz);							// wprowadzenie bicia


	void zapis_wyniku(int wygrany);							//zapis informacji, kto wygral rozgrywke

	int odczyt_wyniku();									//odczyt wynikow graczy

	void zapis_gry(int gracz, int pionkiO, int pionkiX);	//zapis stanu gry, czyli ulozenie pionkow na planszy, ilosc pionkow, oraz na ktorym graczu zakonczono

	int odczyt_gry();										//odczyt stanu gry

	////

	void zapis_trybu();										//zapis i odczyt trybu gry, czyli czlowiek-czlowiek, lub czlowiek-komputer, jest konieczny,
															// by ustawic wskaznik polimorficzny na dana klase, zanim program wczyta plik stanu gry															
	int odczyt_trybu();

};

//tryb czlowiek - komputer
class Komputer : public Gracz
{
public:

	Komputer();

	int ruch_losowy(int gracz);								//tylko w trybie czlowiek-komputer, wykonuje ruch (w tym bicie) komputera

	int podwojne_bicie_komputer(int gracz);					//wprowadza bicie wielokrotne komputera, jesli takowe wystepuje
};

// tryb czlowiek - czlowiek
class Czlowiek : public Gracz
{
public:
	Czlowiek();
};